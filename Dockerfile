# Usa una imagen base de Ubuntu
FROM ubuntu:latest

# Instala sshpass y openssh-client
RUN apt-get update && apt-get install -y sshpass openssh-client

# Copia el script al contenedor
COPY bandit0to7.sh /bandit0to7.sh

# Da permisos de ejecución al script
#RUN chmod +x /bandit0to7.sh

# Ejecuta el script cuando se inicie el contenedor
CMD ["/bandit0to7.sh"]